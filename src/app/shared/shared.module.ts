import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FingerprintComponent } from './fingerprint/fingerprint.component';
import { TableComponent } from './table/table.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        FingerprintComponent,
        TableComponent
    ],
    exports: [
        FingerprintComponent,
        TableComponent
    ]
})

export class SharedModule { }
