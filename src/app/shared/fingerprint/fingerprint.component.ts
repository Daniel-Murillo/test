import { Component, OnInit } from '@angular/core';

export interface Fingerprint {
    parent?: string;
    child?: string;
    childChild?: string;
}

@Component({
    moduleId: module.id,
    selector: 'app-fingerprint',
    templateUrl: './fingerprint.html',
    styleUrls: ['./fingerprint.scss']
})

export class FingerprintComponent implements OnInit {
    finger: Fingerprint;

    constructor() {
        this.finger = {
            parent: 'CATÁLOGOS',
            child: 'ARTISTAS'
        };
    }

    ngOnInit() { }
}
