import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-table',
    templateUrl: './table.html',
    styleUrls: ['./table.scss']
})

export class TableComponent {
    @Input() dataTable: object = [];
    @Input() config: object = [];

    @Output() dataChange: EventEmitter<object>;

    constructor() {
        this.dataChange = new EventEmitter<object>();
    }
}
