import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    // useHash: true        // <-- use local/#/home
    // enableTracing: true  // <-- debugging purposes only
  })],
  exports: [RouterModule],
  providers: []
})
export class AppRouting { }
