import { Injectable } from '@angular/core';
import { Router, RouterState, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { routesWithNoAuth } from '../../guard/routesWithNoAuth';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';

export interface Credentials {
  accessToken: string;
  refreshToken: string;
  rememberToken?: string;
  username?: string;
}

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout
 */
@Injectable()
export class AuthenticationService {

  private _credentials: Credentials;

  constructor(private http: HttpClient,
    private router: Router,
    private $cookies: CookieService) {
    this._credentials = {
      accessToken: this.$cookies.get('accessToken'),
      refreshToken: this.$cookies.get('refreshToken'),
      rememberToken: this.$cookies.get('rememberToken')
    };
  }

  /**
   * Authenticates the user.
   * @param {LoginContext} context The login parameters.
   * @return {Observable<Credentials>} The user credentials.
   */
  login(context: LoginContext) {
    const data = 'json=' + JSON.stringify({ claveCliente: 42314423 });

    return this.http.post(
      environment.urlBase + '/validaCampania.go', data, {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      }).map(response => {
        console.log(response);
      },
      // response ? this.setCredentials(response, context.remember) : response,
      error => {
        console.log(error);
      }
      );
  }

  /**
 * Sets the user credentials.
 * The credentials may be persisted across sessions by setting the `remember` parameter to true.
 * Otherwise, the credentials are only persisted for the current session.
 * @param {Credentials= } credentials The user credentials.
 * @param {boolean= } remember True to remember credentials across sessions.
 */
  private setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || undefined;

    if (credentials.accessToken) {
      const refreshTime = remember ? undefined : undefined;
      this.$cookies.put('accessToken', credentials.accessToken, { expires: refreshTime });
      this.$cookies.put('refreshToken', credentials.refreshToken, { expires: refreshTime });
      this.$cookies.put('rememberToken', credentials.rememberToken, { expires: refreshTime });
      this.router.navigate(['/home'], { replaceUrl: true });
    } else {
      this.logout();
    }
  }

  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */

  logout() {
    this.$cookies.removeAll();
    if (!routesWithNoAuth.find(rute => rute === this.router.url)) {
      this.router.navigate(['/login'], { replaceUrl: true });
    }
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return this.$cookies.get('accessToken') ? true : false;
  }

  /**
   * Gets the user credentials.
   * @return {Credentials} The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials {
    return this._credentials;
  }
}
