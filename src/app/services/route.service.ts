import { Routes } from '@angular/router';

import { ShellComponent } from '../components/shell/layout.component';

/**
 * Provides helper methods to create routes.
 */
export class Route {

  /**
   * Creates routes using the shell component and authentication.
   * @param routes The routes to add.
   * @return {Routes} The new routes using shell as the base.
   */
  static withShell(routes: Routes, father?: any): Routes {
    const route = [{
      path: '',
      component: ShellComponent,
      children: undefined,
      canActivate: []
    }];

    if (father) {
      route[0].children = [{
        path: father.path,
        component: father.component,
        children: routes
      }];
    } else {
      route[0].children = routes;
    }

    return route;
  }

}
