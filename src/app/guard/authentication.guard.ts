import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../services/authentication/authentication.service';

import { routesWithNoAuth } from './routesWithNoAuth';

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (this.authenticationService.isAuthenticated()) {
      return true;
    } else {
      if (!routesWithNoAuth.find(rute => rute === state.url)) {
        console.log('toLogin');
        this.router.navigate(['/login'], { replaceUrl: true });
      }
      return false;
    }
  }
}
