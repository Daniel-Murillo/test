import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../services/authentication/authentication.service';
import { routesWithNoAuth } from './routesWithNoAuth';

@Injectable()
export class NoAuthenticationGuard implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (this.authenticationService.isAuthenticated()) {
      if (routesWithNoAuth.find(rute => rute === state.url)) {
        this.router.navigate(['/home'], { replaceUrl: true });
      }
    }
    return true;
  }
}
