import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

// ROUTING
import { AppRouting } from './app.routing';

// APP MODULES
import { CoreModule } from './core/core.module';
import { FourOFourModule } from './components/404/404.module';

// SHARED
import { SharedModule } from './shared/shared.module';
import { DirectivesModule } from './directives/directives.module';

// LOGIN
import { LoginModule } from './components/login/login.module';
import { RegisterModule } from './components/register/register.module';

// APP
// import { HomeModule } from './components/home/home.module';
// import { UsersModule } from './components/users/users.module';
// import { CatalogueModule } from './components/catalogue/catalogue.module';

// BOOTSTRAP
import { AppComponent } from './app.component';

@NgModule({
  imports: [
    // MODULE FOR RENDER A BROWSER APLICATION
    BrowserModule,
    BrowserAnimationsModule,
    // APPS MODULES
    CoreModule,
    SharedModule,
    DirectivesModule,
    FourOFourModule,
    LoginModule,
    RegisterModule,
    // APP
    // HomeModule,
    // UsersModule,
    // CatalogueModule,
    // DEFINE AT LAST AppRouting because the other routes can be defined in the root
    AppRouting
  ],
  declarations: [
    // ROOT COMPONENT
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
