export class LogIn {
    accessToken: string;
    refreshToken: string;
    type: string;
}
