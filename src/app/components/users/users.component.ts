import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: './users.html',
    styleUrls: ['./users.scss']
})

export class UsersComponent {}
