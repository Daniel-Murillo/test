import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRouting } from './users.routing';
import { UsersComponent } from './users.component';

@NgModule({
    imports: [
        CommonModule,
        UsersRouting
    ],
    declarations: [
        UsersComponent
    ],
    providers: []
})

export class UsersModule {}
