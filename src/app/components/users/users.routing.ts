import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { Route } from '../../services';
import { UsersComponent } from './users.component';

const routes: Routes = Route.withShell([
    { path: 'users', component: UsersComponent }
]);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class UsersRouting { }
