import { NgModule } from '@angular/core';

import { FourOFourRouting } from './404.routing';
import { FourOFourComponent } from './404.component';

@NgModule({
    imports: [
        FourOFourRouting
      ],
      declarations: [
        FourOFourComponent
      ],
})
export class FourOFourModule { }
