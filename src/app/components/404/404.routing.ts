import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FourOFourComponent } from './404.component';

const routes: Routes = [{ path: '404', component: FourOFourComponent, data: { title: '404 NotFound'} }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class FourOFourRouting { }
