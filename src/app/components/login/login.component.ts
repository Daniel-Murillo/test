import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthenticationService, AlertService } from '../../services';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['./login.scss']
})

export class LoginComponent implements OnInit {
    model: any = {};
    inputFocus = {
        username: false,
        password: false
    };

    errorLogin = {
        status: 0,
        message: ''
    };

    loading = false;
    returnUrl: string;
    loginForm: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private formBuilder: FormBuilder) {
        this.createForm();
    }

    ngOnInit() { }

    activeInput(input) {
        this.inputFocus[input] = true;
    }

    blurInput(input) {
        if (this.loginForm.controls[input].value.length === 0) {
            this.inputFocus[input] = false;
        }
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.loginForm.value)
            .finally(() => { this.loading = false; })
            .subscribe(
            data => {
                console.log(data);
            },
            error => {
                if (error.status === 200) {
                    error.messsage = this.errorLogin.message;
                } else {
                    this.loginForm.controls.password.reset('');
                    this.errorLogin = {
                        status: 402,
                        message: 'E-mail o contraseña incorrecta'
                    };
                }
            });
    }

    private createForm() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            remember: true
        });
        // console.log(this.loginForm);
    }
}
