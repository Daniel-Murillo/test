import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// import { TranslateModule } from '@ngx-translate/core';

import { LoginRouting } from './login.routing';
import { LoginComponent } from './login.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LoginRouting
  ],
  declarations: [
    LoginComponent
  ]
})
export class LoginModule { }
