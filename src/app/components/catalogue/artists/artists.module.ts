import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { async, TestBed, fakeAsync, tick } from '@angular/core/testing';

// ARTIST COMPONENT AND ROUTING
import { ArtistsComponent } from './artists.component';
import { ArtistsRoutingModule } from './artists.routing';

// SERVICES
import { ArtistService } from './services/artists.service';

// SHARED
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ArtistsRoutingModule
    ],
    declarations: [
        // Components for ArtistsModule
        ArtistsComponent
    ],
    providers: [
        // Services only for Artists
        ArtistService
    ]
})

export class ArtistsModule { }
