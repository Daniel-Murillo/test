import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

// import { ArtistsModel } from '../models/artist.model';
export interface ArtistsModel {
    id: number;
    name: string;
    city?: string;
    email?: string;
    phone?: string;
    status: number;
    canEdit: boolean;
    canErase: boolean;
}

@Injectable()
export class ArtistService {
    constructor(private http: HttpClient) { }

    get(...searchArtist: any[]) {
        return this.http.get('/artists')/*.map(data => console.log(data), error => console.log(error))*/;
    }

    create(artist: ArtistsModel) {
        return this.http.post('/artists', artist)/*.map(data => data)*/;
    }

    update(artist: ArtistsModel) {
        return this.http.put(`/artists/${artist.id}`, artist)
            .map((response: Response) => response.json());
    }

    delete(artistId: number) {
        return this.http.delete(`/artists/${artistId}`)
            .map((response: Response) => response.json());
    }
}
