import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ArtistService } from './services/artists.service';

import { ToastrService } from 'ngx-toastr';

export interface Artists {
    id: number;
    name: string;
    city?: string;
    email?: string;
    phone?: string;
    status: number;
    canEdit: boolean;
    canErase: boolean;
}

@Component({
    moduleId: module.id,
    templateUrl: 'artists.component.html',
    styleUrls: ['./artists.scss'],
    providers: [ArtistService]
})

export class ArtistsComponent implements OnInit {
    dataTable: any;
    loadTable: boolean;

    constructor(private http: HttpClient,
        private artistService: ArtistService,
        private toastr: ToastrService) {
        this.dataTable = {
            loadTable: false,
            fields: [
                { property: 'name', title: 'ARTISTA' },
                { property: 'city', title: 'CIUDAD' },
                { property: 'email', title: 'E-MAIL' },
                { property: 'phone', title: 'TELÉFONO' },
                { property: 'status', title: 'STATUS', class: 'text-center'},
                { property: 'options', title: '', class: 'text-center' }
            ],
            data: []
        };
    }

    ngOnInit() {
        this.getArtists();
        // this.toastr.success('texto, texto añdofijañsoijfañsdf', 'Title');
        // this.toastr.error('texto, texto añdofijañsoijfañsdf', 'Title');
        // this.toastr.info('texto, texto añdofijañsoijfañsdf', 'Title');
    }

    getArtists(searchOptions?: any) {
        this.dataTable.loadTable = true;

        this.artistService.get(searchOptions)
            .finally(() => {
                this.dataTable.loadTable = false;
            })
            .subscribe(
            data => {
                this.dataTable.data = data;
            },
            error => console.log(error));
    }
}
