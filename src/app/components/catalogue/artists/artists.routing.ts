import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { Route } from '../../../services';
import { ArtistsComponent } from './artists.component';

import { CatalogueComponent } from '../catalogue.component';

const routes: Routes = Route.withShell([
  { path: '', redirectTo: 'artists', pathMatch: 'full' },
  { path: 'artists', component: ArtistsComponent }
], { path: 'catalogue', component: CatalogueComponent });

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})

export class ArtistsRoutingModule { }
