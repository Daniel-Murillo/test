export interface ArtistsModel {
    id: number;
    name: string;
    city?: string;
    email?: string;
    phone?: string;
    status: number;
    canEdit: boolean;
    canErase: boolean;
}
