import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../../services/route.service';
import { CatalogueComponent } from './catalogue.component';

const routes: Routes = Route.withShell([
    { path: 'catalogue', component: CatalogueComponent }
]);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class CatalogueRoutingModule { }
