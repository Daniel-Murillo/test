import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: './catalogue.html',
    styleUrls: ['./catalogue.scss']
})

export class CatalogueComponent { }
