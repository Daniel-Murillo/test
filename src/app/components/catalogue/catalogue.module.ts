import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';

// ROUTING
import { CatalogueRoutingModule } from './catalogue.routing';

// APP MODULES
import { ArtistsModule } from './artists/artists.module';
import { CatalogueComponent } from './catalogue.component';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        // APPS MODULES
        CoreModule,
        ArtistsModule,
        CatalogueRoutingModule,
        SharedModule
    ],
    declarations: [
        // ROOT COMPONENT
        CatalogueComponent
    ],
    providers: [
        // FUNCTIONS ONLY FOR CATALOGUE MODULE
    ],
    bootstrap: []
})
export class CatalogueModule { }
