import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../../services/route.service';
import { HomeComponent } from './home.component';

const routes: Routes = Route.withShell([
  { path: 'home', component: HomeComponent }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule { }
