import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register.component';
import { NoAuthenticationGuard } from '../../guard/noAuthentication.guard';

const routes: Routes = [{
  path: 'registrar',
  component: RegisterComponent,
  data: { title: 'Registro ' }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [NoAuthenticationGuard]
})
export class RegisterRouting { }
