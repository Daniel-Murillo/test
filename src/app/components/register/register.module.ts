import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// import { TranslateModule } from '@ngx-translate/core';

import { RegisterRouting } from './register.route';
import { RegisterComponent } from './register.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // TranslateModule,
    RegisterRouting
  ],
  declarations: [
    RegisterComponent
  ]
})
export class RegisterModule { }
