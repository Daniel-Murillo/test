import 'rxjs/add/operator/finally';

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { AlertService, UserService } from '../../services';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html',
    styleUrls: ['./register.scss']
})

export class RegisterComponent {
    loading = false;
    registerForm: FormGroup;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private formBuilder: FormBuilder) {
            this.creatRegisterForm();
        }

    creatRegisterForm() {
        this.registerForm = this.formBuilder.group({
            username: new FormControl('', [Validators.required, Validators.minLength(4),
                // forbiddenNameValidator(/bob/i) // <-- Here's how you pass in the custom validator.
              ]),
            lastname: ['', Validators.required],
            password: ['', Validators.required],
            repeatPassword: ['', Validators.required]
          }, this.passwordMatchValidator);
    }

    passwordMatchValidator(g: FormGroup) {
        return g.get('password').value === g.get('passwordConfirm').value
           ? null : {'mismatch': true};
     }

    register() {
        this.loading = true;
        this.userService.create(this.registerForm.value)
            .finally(() => {
                this.loading = false;
            }).subscribe(
            data => {
                this.alertService.success('Registration successful', true);
                this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error(error);
            });
    }
}
