import { NgModule, Optional, SkipSelf } from '@angular/core';

//  NECESARY ANGULAR MODULES
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

// INTERCEPTOR
import { InterceptorsModule } from './http/interceptor.module';

// THIRD PARTYYYYYYYY ANGULAR MODULES
import { CookieModule } from 'ngx-cookie';
// import { MetaGuard } from '@ngx-meta/core';

// NOTIFICATION
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SidebarModule } from 'ng-sidebar';

// SHELL COMPONENTS
import { ShellComponent } from '../components/shell/layout.component';
import { HeaderComponent } from '../components/shell/navbar/navbar.component';
import { SidebarComponent } from '../components/shell/sidebar/sidebar.component';

// SERVICES
import { AlertService } from '../services/alert.service';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { UserService } from '../services/user.service';

// GUARDS
import { NoAuthenticationGuard } from '../guard/noAuthentication.guard';
import { AuthenticationGuard } from '../guard/authentication.guard';

@NgModule({
  imports: [
    // FUNCTIONAL MODULES
    RouterModule,
    FormsModule,
    HttpClientModule,
    InterceptorsModule,
    CookieModule.forRoot(),
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added,
    SidebarModule.forRoot()
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    ShellComponent
  ],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
    AlertService,
    UserService,
    NoAuthenticationGuard
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }

}
