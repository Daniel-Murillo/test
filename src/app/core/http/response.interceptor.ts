import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { Router } from '@angular/router';

import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { AuthenticationService } from '../../services/authentication/authentication.service';


@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).do((event: HttpEvent<any>) => {
      // console.log(event);
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
          // console.log(event.status);
          if (event.status === 200) {
            // console.log('all right');
          }
        } else if (event instanceof HttpErrorResponse) {
          // console.log(event.status);
          switch (event.status) {
            case 404:
              console.log('rute not found');
              break;
          }
        } else if (event === undefined) {
          console.log('ERR_CONNECTION_REFUSED');
        }
      });
  }
}
