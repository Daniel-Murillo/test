import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { environment } from '../../../environments/environment';

// import { AuthenticationService } from '../../services/authentication/authentication.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor(/* private auth: AuthenticationService */) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const JWT = `Bearer dasdadasdas`;
        // Put URLBASE in url
        if (req.url[0] === '/') {
            req = req.clone({
                url: `${environment.urlBase + req.url}`,
                setHeaders: {
                    Authorization: JWT
                }
            });
        }
        return next.handle(req);
    }
}
