import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { RequestInterceptor } from './request.interceptor';
import { ResponseInterceptor } from './response.interceptor';

// import { AuthenticationService } from '../../services/authentication/authentication.service';

@NgModule({
    providers: [
        // AuthenticationService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true }
    ]
})

export class InterceptorsModule { }
